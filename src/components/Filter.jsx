import React from "react";
import {FASHION_TYPE, SPORT_TYPE} from "./ArticleList";
import styled from "styled-components";

const StyledFilter = styled.div`
  display: flex;
  flex: 1;
  flex-flow: row wrap;
  height: 15vh;
  min-width: 20vh;
  font-size: 1em;
  color: #4f7962;
  @media(max-width: 768px) {
    flex-direction: column;
  }
`;

const FilterLabel = styled.div`
  font-weight: bold;
  padding-bottom: 1em;
`;

const CheckboxWrapper = styled.div`
  display: flex;
  flex-direction: column;
  @media(max-width: 768px) {
    flex-direction: row;
  }
`;

const ChecklistItem = styled.input`
  width: 1.1em;
  height: 1.1em;
  margin-right: 1em;
`;

const ChecklistLabel = styled.label`
  text-transform: capitalize;
  line-height: 2em;
  margin-right: 0.5em;
`

const Filter = ({filterArray, handleFilter}) => {
    return <StyledFilter>
        <FilterLabel>Data sources</FilterLabel>
        <CheckboxWrapper>
            {[FASHION_TYPE, SPORT_TYPE].map((filterType)=>{
                return <ChecklistLabel key={filterType}>
                    <ChecklistItem type="checkbox" checked={filterArray.includes(filterType)} onChange={() => handleFilter(filterType)} />
                    {filterType}
                </ChecklistLabel>
            })
            }
        </CheckboxWrapper>
    </StyledFilter>
};

export default Filter;