import React from 'react';
import {render} from '@testing-library/react';
import Article from './Article';
import '@testing-library/jest-dom';

const article = {
    id: 123544,
    date: '1. oktober 2018',
    image: 'https://placeimg.com/280/180/nature',
    category: 'sport',
    title: 'Solskjær fikk klar beskjed fra Røkke og Gjelsten: – Ikke kom tilbake!',
    preamble: 'Ole Gunnar Solskjær forteller om den spesielle samtalen med de to Molde-investorene.'
};

describe("Article Component", function () {
    it('should display article title', () => {
        const {getByText} = render(
            <Article article={article} />,
        );
        expect(getByText(article.title)).toBeInTheDocument();
    });
});
