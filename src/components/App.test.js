import React from "react";
import { render } from "@testing-library/react";
import App from "./App";
import '@testing-library/jest-dom';

describe("App Component", function () {
    it("should have Karolina message", function () {
        let { getByText } = render(<App />);
        expect(getByText("Karolina Schibsted Assignement")).toBeInTheDocument();
    });
});