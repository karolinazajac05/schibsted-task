import styled from "styled-components";
import React from "react";
import Placeholder from '.././assets/placeholder.png';

const ArticleItem = styled.div`
  display: flex;
  font-size: 1.5em;
  border-radius: 0.5em;
  background-color: #f6f6f6;
  box-shadow: 0.2em 0.2em 0.2em #d2dfd2;
  padding: 1em;
  justify-content: space-between;
`;
const ImgWrapper = styled.div`
  display: flex;
`;

const Image = styled.img`
  width: 200px;
  min-height: 100px;
  object-fit: contain;
  padding-right: 1em;
  @media (max-width: 768px) {
    width: 150px;
  }
`;

const Content = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: baseline;
  width: 100%;
`;

const Title = styled.div`
  font-size: 0.9em;
  font-weight: bold;
  color: #a1b9a1;
  width: calc(100% - 7em);
  @media (max-width: 768px) {
    width: 100%;
    font-size: 0.7em;
  }
`;

const Date = styled.div`
  font-size: 0.6em;
`;

const Preamble = styled.div`
  text-overflow: ellipsis;
  font-size: 0.7em;
  min-width: 80%;
  flex-grow: 2;
  @media (max-width: 768px) {
    display: none;
  }
`;

const Article = ({article}) => {
    return <ArticleItem>
        <ImgWrapper>
            <Image src={article.image} onError={({ currentTarget }) => {
                currentTarget.onerror = null;
                currentTarget.src = Placeholder;
            }} alt={"Article image preview"}/>
        </ImgWrapper>
        <Content>
            <Title>
                {article.title}
            </Title>
            <Date>
                {article.date}
            </Date>
            <Preamble>
                {article.preamble}
            </Preamble>
        </Content>
    </ArticleItem>
}

export default Article;