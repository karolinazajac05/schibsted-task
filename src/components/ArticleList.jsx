import React, {useEffect, useMemo, useState} from 'react';
import axios from "axios";
import styled from "styled-components";
import Article from "./Article";
import Filter from "./Filter";
const { DateTime } = require("luxon");

const ArticlePage = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1em;
`;

const Sort = styled.div`
  display: flex;
  font-size: 1em;
  text-align: center;
  color: #4f7962;
  align-self: flex-end;
  padding-right: 0.5em;
  padding-bottom: 1em;
  cursor: pointer;
  @media (max-width: 768px) {
    position: absolute;
  }
`;

const SortIcon = styled.i`
  width: 0;
  height: 0;
  border: solid 5px transparent;
  margin: 8px 4px 0 10px;
  background: transparent;
  border-top: solid 7px #519461;
  border-bottom-width: 0;
  transform: ${props => props.desc ? "none" : "rotate(0.5turn)"};
`;

const ArticleListWrapper = styled.div`
  display: flex;
  gap: 1em;
  flex: 5;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

const Feed = styled.div`
  display: flex;
  flex-direction: column;
  gap: 1em;
  flex-grow: 3;
`;

const ArticlesPlaceholder = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: center;
  font-size: 2em;
  color: #4f7962;
`;
export const FASHION_TYPE = 'fashion';
export const SPORT_TYPE = 'sport';

const ArticleList = () => {
    const [articles, setArticles] = useState([]);
    const [filterArray, setFilterArray] = useState([FASHION_TYPE, SPORT_TYPE]);
    const [sortDesc, setSortDesc] = useState(true);

    useEffect(()=> {
        const fetchFashion = axios.get("/articles/fashion");
        const fetchSport = axios.get("/articles/sports");

        Promise.all([fetchFashion, fetchSport]).then((response) => {
            const fashionType = response[0].data.articles;
            const sportType = response[1].data.articles;
            const allArticles = fashionType.concat(sportType).sort((a, b)=> {
                const dateFormat = "d. LLLL yyyy";
                return DateTime.fromFormat(b.date, dateFormat,{ locale: "no" }) - DateTime.fromFormat(a.date, dateFormat, { locale: "no" });
            });
            setArticles(allArticles);
        }).catch(error => {
        });
    }, [])

    const filterByType = value => {
        return filterArray.includes(value.category);
    }

    const handleSort = () => {
        setSortDesc(!sortDesc);
    }

    const handleFilter = (filterType) => {
        const newFilter = filterArray.includes(filterType) ?  filterArray.filter((item) => item !== filterType) : filterArray.concat([filterType]);
        setFilterArray(newFilter);
    }

    const filteredArticles = useMemo(() => {
        const filteredData = articles.filter(filterByType);
        return sortDesc ? filteredData : filteredData.reverse();
    }, [articles, filterArray, sortDesc])

    return (
        <ArticlePage>
            <Sort onClick={handleSort}>Sort by date <SortIcon desc={sortDesc}/></Sort>
            <ArticleListWrapper>
                <Filter filterArray={filterArray} handleFilter={handleFilter}/>
                {filteredArticles.length > 0 ? <Feed>
                        {filteredArticles.map(article => {
                            return (
                                <Article key={article.type + '_' + article.id} article={article}/>
                            );
                        })
                        }
                    </Feed> :
                    <ArticlesPlaceholder>No articles here...</ArticlesPlaceholder>
                }
            </ArticleListWrapper>
        </ArticlePage>
    );
};

export default ArticleList;