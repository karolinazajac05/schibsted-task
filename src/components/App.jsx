import React from 'react'
import ArticleList from "./ArticleList";
import styled from "styled-components";

const AppContainer = styled.div`
  font-family: 'Open Sans', sans-serif;
`;

const Header = styled.h2`
  display: flex;
  align-items: center;
  justify-content: center;
  text-transform: uppercase;
  font-weight: bold;
  text-align: center;
  padding: 1em 0;
  color: #6f946f;
`;
const App = () => (
    <AppContainer>
        <Header>Karolina Schibsted Assignement</Header>
        <ArticleList/>
    </AppContainer>
)

export default App;