## Final note

I have created a React app which is fetching data from server. 
I added proxy to fetch data from endpoints without path.

I added simple setup for tests (I am not experienced in writing frontend tests, so I think that this is a place where 
I would like to enhance my app). 

For styling, I added styled-components package, because I am familiar with it and
I think for such simple app it is a good choice, because we don't need overall styles, also it is easier to check 
styling. 

Beside adding more tests I think that eslint and prettier packages could be added for ensuring that we have well 
formatted project.

Because this assignment should take only few hours you can definitely enhance UX experience with more time spend on 
design.

For bigger database of articles is would be good to add lazy loading/pagination for articles.

There could be also a good idea to add filter parameter to url to remember selected filters.